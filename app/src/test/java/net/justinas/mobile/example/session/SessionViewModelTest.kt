package net.justinas.mobile.example.session

import net.justinas.mobile.example.TricksRepository
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import net.justinas.mobile.example.domain.TrickListItem
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SessionViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun tests2WorkedTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(2, name = "What2", number = 4),
            TrickListItem(3, name = "What3", number = 5),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 3)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 1 * 5)
        Assert.assertEquals(sessionList.coolDown.size, 1 * 5)
    }

    @Test
    fun tests0WorkedManyNewTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 3)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 0)
        Assert.assertEquals(sessionList.coolDown.size, 0)
    }

    @Test
    fun tests1WorkedTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(2, name = "What2", number = 4),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 3)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 1 * 5)
    }

    @Test
    fun tests7WorkedTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(2, name = "What2", number = 4),
            TrickListItem(3, name = "What3", number = 5),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 4),
            TrickListItem(11, name = "What11", number = 4),
            TrickListItem(12, name = "What12", number = 4),
            TrickListItem(13, name = "What13", number = 4),
            TrickListItem(14, name = "What14", number = 5)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 4 * 5)
        Assert.assertEquals(sessionList.coolDown.size, 3 * 5)
    }
    @Test
    fun tests8WorkedTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(2, name = "What2", number = 4),
            TrickListItem(3, name = "What3", number = 5),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 4),
            TrickListItem(11, name = "What11", number = 4),
            TrickListItem(12, name = "What12", number = 4),
            TrickListItem(13, name = "What13", number = 4),
            TrickListItem(14, name = "What14", number = 5),
            TrickListItem(15, name = "What15", number = 5)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 4 * 5)
        Assert.assertEquals(sessionList.coolDown.size, 4 * 5)
    }

    @Test
    fun tests9WorkedTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(1, name = "What1", number = 3),
            TrickListItem(2, name = "What2", number = 4),
            TrickListItem(3, name = "What3", number = 5),
            TrickListItem(4, name = "What4", number = 3),
            TrickListItem(5, name = "What5", number = 0),
            TrickListItem(6, name = "What6", number = 1),
            TrickListItem(7, name = "What7", number = 1),
            TrickListItem(8, name = "What8", number = 2),
            TrickListItem(9, name = "What9", number = 2),
            TrickListItem(10, name = "What10", number = 4),
            TrickListItem(11, name = "What11", number = 4),
            TrickListItem(12, name = "What12", number = 4),
            TrickListItem(13, name = "What13", number = 4),
            TrickListItem(14, name = "What14", number = 5),
            TrickListItem(15, name = "What15", number = 5),
            TrickListItem(16, name = "What15", number = 5)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertNotEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 4 * 5)
        Assert.assertEquals(sessionList.coolDown.size, 4 * 5)
    }

    @Test
    fun tests0Worked0NormalTricks() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = emptyList<TrickListItem>()

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertEquals(sessionList.normal.id, 0)

        Assert.assertEquals(sessionList.warmUp.size, 0)
        Assert.assertEquals(sessionList.coolDown.size, 0)
    }

    @Test
    fun tests0Worked1NewTrick() {
        val viewModel = SessionViewModel(TricksRepository())
        val testResult = listOf(
            TrickListItem(10, name = "What10", number = 3)
        )

        val sessionList = viewModel.getSessionList(testResult)

        Assert.assertNotEquals(sessionList.normal.number, 4)
        Assert.assertNotEquals(sessionList.normal.number, 5)
        Assert.assertEquals(sessionList.normal.id, 10)

        Assert.assertEquals(sessionList.warmUp.size, 0)
        Assert.assertEquals(sessionList.coolDown.size, 0)
    }
}