package net.justinas.mobile.example.session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.mobile.example.databinding.FragmentSessionBinding
import net.justinas.mobile.example.databinding.FragmentTricksBinding
import net.justinas.mobile.example.domain.TrickListItem
import net.justinas.mobile.example.tricks.ListItemAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class SessionFragment : Fragment(), ListItemAdapter.Callbacks {

    override fun onItemClick(view: View, item: TrickListItem) {

    }

    private val viewModel: SessionViewModel by viewModel()
    private lateinit var binding: FragmentSessionBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        binding = FragmentSessionBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }





}
