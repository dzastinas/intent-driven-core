package net.justinas.mobile.example.util

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import net.justinas.mobile.core.util.LoadResult
import net.justinas.mobile.example.domain.TrickListItem
import net.justinas.mobile.example.tricks.ListItemAdapter


object BindingAdapter {

    @JvmStatic
    @BindingAdapter("listItemAdapter", "idEntityCallback", requireAll = false)
    fun RecyclerView.setReviewAdapter(
        result: LoadResult<List<TrickListItem>>?,
        callback: ListItemAdapter.Callbacks
    ) {
        val productAdapter = ListItemAdapter(callback)
        adapter = productAdapter

        if (result is LoadResult.Success) {
            productAdapter.items = result.data
        }
    }

}