package net.justinas.mobile.example.session

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.justinas.mobile.example.TricksRepository
import net.justinas.mobile.example.domain.TrickListItem
import net.justinas.mobile.example.domain.SessionList
import net.justinas.mobile.core.util.LoadResult

class SessionViewModel(private val repository: TricksRepository) : ViewModel() {

    val result = MutableLiveData<LoadResult<List<TrickListItem>>>()

    init {
        loadList()
    }

    private fun loadList() {
        result.postValue(LoadResult.Success(getSessionList(repository.getAllList()).toListWithHeaders()))
    }

    fun retry() {
        loadList()
    }


    fun getSessionList(allList: List<TrickListItem>): SessionList {
        if (allList.isEmpty()) return SessionList()

        val warmUpList = mutableListOf<TrickListItem>()
        val coolDownList = mutableListOf<TrickListItem>()
        var normalTraining = TrickListItem()

        val knownList = allList.filter { it.number >= 4 }
        val newList = allList.minus(knownList)

        if (newList.isNotEmpty()) normalTraining = newList.shuffled().first()

        when {
            knownList.size in 1..7 -> {
                val shuffled = knownList.shuffled()
                warmUpList.addAll(shuffled.take((knownList.size + 1) / 2))
                coolDownList.addAll(shuffled.minus(warmUpList))
            }
            knownList.size >= 8 -> {
                val shuffled = knownList.shuffled().take(8)
                warmUpList.addAll(shuffled.take(4))
                coolDownList.addAll(shuffled.minus(warmUpList))
            }
        }
        val shallowCopy =  warmUpList.toMutableList()
        warmUpList.addAll(shallowCopy)
        warmUpList.addAll(shallowCopy)
        warmUpList.addAll(shallowCopy)
        warmUpList.addAll(shallowCopy)
        warmUpList.shuffle()

        val shallowCopy2 =  coolDownList.toMutableList()
        coolDownList.apply {
            addAll(shallowCopy2)
            addAll(shallowCopy2)
            addAll(shallowCopy2)
            addAll(shallowCopy2)
            shuffle()
        }

        return SessionList(warmUpList, normalTraining, coolDownList)
    }


}
