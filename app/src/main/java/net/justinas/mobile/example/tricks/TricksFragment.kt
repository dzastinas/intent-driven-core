package net.justinas.mobile.example.tricks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.mobile.example.R
import net.justinas.mobile.example.databinding.FragmentTricksBinding
import net.justinas.mobile.example.domain.TrickListItem
import org.koin.androidx.viewmodel.ext.android.viewModel

class TricksFragment : Fragment(), ListItemAdapter.Callbacks {

    override fun onItemClick(view: View, item: TrickListItem) {}

    private val viewModel: TricksViewModel by viewModel()
    private lateinit var binding: FragmentTricksBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        binding = FragmentTricksBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        binding.startSession.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.startSessionFragment)
        }
        return binding.root
    }
}
