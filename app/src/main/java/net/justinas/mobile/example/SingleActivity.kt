package net.justinas.mobile.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.NavigationUI

class SingleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return

        NavigationUI.setupActionBarWithNavController(this, host.navController)
    }

    override fun onSupportNavigateUp() = findNavController(supportFragmentManager
        .findFragmentById(R.id.nav_host_fragment)!!).navigateUp()
}
