package net.justinas.mobile.example.domain

import java.io.Serializable

data class TrickListItem(
    val id: Long = 0,
    val name: String = "",
    val type: Int = 0,
    val number: Int = 0
) :
    Serializable