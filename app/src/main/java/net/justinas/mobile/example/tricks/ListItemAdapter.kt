package net.justinas.mobile.example.tricks

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_trick.view.*
import net.justinas.mobile.core.util.AutoUpdatableAdapter
import net.justinas.mobile.example.databinding.ListItemTrickBinding
import net.justinas.mobile.example.domain.TrickListItem
import kotlin.properties.Delegates

class ListItemAdapter(private val callbacks: Callbacks? = null) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    AutoUpdatableAdapter {

    interface Callbacks {
        fun onItemClick(view: View, item: TrickListItem)
    }

    var items: List<TrickListItem> by Delegates.observable(emptyList()) { _, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id && o.name == n.name}
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            0 -> {
                val binding = ListItemTrickBinding.inflate(inflater, parent, false)
                ViewHolder(binding)
            }
            else -> {
                val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
                HeaderViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.binding.item = items[position]
                holder.binding.executePendingBindings()
            }
            is HeaderViewHolder -> {
                holder.view.findViewById<TextView>(android.R.id.text1).text = items[position].name
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    override fun getItemCount(): Int = items.size


    inner class ViewHolder(val binding: ListItemTrickBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener { callbacks?.onItemClick(it, items[adapterPosition]) }
        }
    }

    inner class HeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}