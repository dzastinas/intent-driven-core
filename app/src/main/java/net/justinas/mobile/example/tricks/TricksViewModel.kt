package net.justinas.mobile.example.tricks

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.justinas.mobile.example.TricksRepository
import net.justinas.mobile.example.domain.TrickListItem
import net.justinas.mobile.core.util.LoadResult

class TricksViewModel(private val repository: TricksRepository) : ViewModel() {

    val result = MutableLiveData<LoadResult<List<TrickListItem>>>()

    init {
        loadList()
    }

    private fun loadList() {
        result.postValue(LoadResult.Success(repository.getAllList()))
    }

    fun retry() {
        loadList()
    }

}
