package net.justinas.mobile.example

import net.justinas.mobile.example.domain.TrickListItem

class TricksRepository {
    private val list = listOf(
        TrickListItem(id = 1, name = "Name", number = 5),
        TrickListItem(id = 2, name = "Clicker", number = 5),
        TrickListItem(id = 3, name = "Sit", number = 3),
        TrickListItem(id = 4, name = "Down", number = 4),
        TrickListItem(id = 5, name = "Stand", number = 0),
        TrickListItem(id = 6, name = "Come", number = 0),
        TrickListItem(id = 7, name = "Spin", number = 0),
        TrickListItem(id = 8, name = "Touch", number = 0),
        TrickListItem(id = 9, name = "Touch2", number = 1),
        TrickListItem(id = 10, name = "Touch3", number = 2),
        TrickListItem(id = 11, name = "Jump", number = 5),
        TrickListItem(id = 12, name = "Fly", number = 4),
        TrickListItem(id = 13, name = "Sit & Wait", number = 0)
    )

    fun getAllList(): List<TrickListItem> {
        return list
    }

}
