package net.justinas.mobile.example

import android.app.Application
import net.justinas.mobile.example.session.SessionViewModel
import net.justinas.mobile.example.tricks.TricksViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class BasicApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BasicApplication)

            val myModule = module {
                single { TricksViewModel(get()) }
                single { SessionViewModel(get()) }
                single { TricksRepository() }
            }

            modules(myModule)
        }
    }
} 