package net.justinas.mobile.example.domain

data class SessionList(
    val warmUp: List<TrickListItem> = emptyList(),
    val normal: TrickListItem = TrickListItem(),
    val coolDown: List<TrickListItem> = emptyList()
) {
    fun isEmpty() = warmUp.isEmpty() && normal.id == 0L && coolDown.isEmpty()
    fun toListWithHeaders(): List<TrickListItem> {
        return getListWithHeader(warmUp, "Warm Up") + getListWithHeader(listOf(normal), "Normal") + getListWithHeader(
            coolDown,
            "Cool Down"
        )
    }

    private fun getListWithHeader(list: List<TrickListItem>, header: String) =
        if (list.isNotEmpty()) {
            listOf(TrickListItem(id = 0, name = header, type = 1)) + list
        } else emptyList()
}