package net.justinas.mobile.core.util

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import net.justinas.mobile.core.domain.IdEntity

object ListViewAdapters {

    @JvmStatic
    @BindingAdapter("listItemPagedAdapter", "idEntityPagedCallback", requireAll = false)
    fun RecyclerView.setPagingReviewAdapter(
        result: List<IdEntity>?,
        callback: IdGridEntityAdapter.Callbacks
    ) {
        if (adapter == null) {
            val idEntityAdapter = IdGridEntityAdapter(callback)
            adapter = idEntityAdapter
        }

        if (adapter != null && result != null && result.isNotEmpty()) {
            (adapter as IdGridEntityAdapter).items = result
        }
    }

    interface OnScrollStateChanged {
        fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)
    }

    interface OnScrolled {
        fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
    }
}
